package auth

import (
	"context"
	"gitlab.com/kuzmem/auth/internal/modules/auth/service"
	"gitlab.com/kuzmem/gateway/pkg/processing/auth"
)

// AuthServiceJSONRPC представляет AuthService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	userService service.Auther
}

// NewAuthServiceJSONRPC возвращает новый AuthServiceJSONRPC
func NewAuthServiceJSONRPC(authService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{userService: authService}
}

func (a *AuthServiceJSONRPC) Register(in auth.RegisterIn, out *auth.RegisterOut) error {
	*out = a.userService.Register(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(in auth.AuthorizeEmailIn, out *auth.AuthorizeOut) error {
	*out = a.userService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(in auth.AuthorizeRefreshIn, out *auth.AuthorizeOut) error {
	*out = a.userService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizePhone(in auth.AuthorizePhoneIn, out *auth.AuthorizeOut) error {
	*out = a.userService.AuthorizePhone(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SendPhoneCode(in auth.SendPhoneCodeIn, out *auth.SendPhoneCodeOut) error {
	*out = a.userService.SendPhoneCode(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) VerifyEmail(in auth.VerifyEmailIn, out *auth.VerifyEmailOut) error {
	*out = a.userService.VerifyEmail(context.Background(), in)
	return nil
}
