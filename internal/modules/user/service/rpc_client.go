package service

import (
	"context"
	"net/rpc"

	"gitlab.com/kuzmem/gateway/pkg/processing/user"

	"gitlab.com/kuzmem/auth/internal/infrastructure/errors"
)

type UserServiceJSONRPC struct {
	client *rpc.Client
}

func NewUserServiceJSONRPC(client *rpc.Client) *UserServiceJSONRPC {
	u := &UserServiceJSONRPC{client: client}

	return u
}

func (t *UserServiceJSONRPC) Create(ctx context.Context, in user.UserCreateIn) user.UserCreateOut {
	var out user.UserCreateOut
	err := t.client.Call("UserServiceJSONRPC.CreateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceCreateUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) Update(ctx context.Context, in user.UserUpdateIn) user.UserUpdateOut {
	var out user.UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.UpdateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) VerifyEmail(ctx context.Context, in user.UserVerifyEmailIn) user.UserUpdateOut {
	var out user.UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) ChangePassword(ctx context.Context, in user.ChangePasswordIn) user.ChangePasswordOut {
	var out user.ChangePasswordOut
	err := t.client.Call("UserServiceJSONRPC.ChangePassword", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceChangePasswordErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByEmail(ctx context.Context, in user.GetByEmailIn) user.UserOut {
	var out user.UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByPhone(ctx context.Context, in user.GetByPhoneIn) user.UserOut {
	var out user.UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByPhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByID(ctx context.Context, in user.GetByIDIn) user.UserOut {
	var out user.UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByIDs(ctx context.Context, in user.GetByIDsIn) user.UsersOut {
	var out user.UsersOut
	err := t.client.Call("UserServiceJSONRPC.GetUsersByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUsersErr
	}

	return out
}
