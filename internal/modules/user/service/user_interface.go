package service

import (
	"context"

	"gitlab.com/kuzmem/gateway/pkg/processing/user"
)

//go:generate easytags $GOFILE

type Userer interface {
	Create(ctx context.Context, in user.UserCreateIn) user.UserCreateOut
	Update(ctx context.Context, in user.UserUpdateIn) user.UserUpdateOut
	VerifyEmail(ctx context.Context, in user.UserVerifyEmailIn) user.UserUpdateOut
	ChangePassword(ctx context.Context, in user.ChangePasswordIn) user.ChangePasswordOut
	GetByEmail(ctx context.Context, in user.GetByEmailIn) user.UserOut
	GetByPhone(ctx context.Context, in user.GetByPhoneIn) user.UserOut
	GetByID(ctx context.Context, in user.GetByIDIn) user.UserOut
	GetByIDs(ctx context.Context, in user.GetByIDsIn) user.UsersOut
}
