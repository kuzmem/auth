package modules

import (
	upb "gitlab.com/kuzmem/gateway/pkg/proto/user_grpc"
	"net/rpc"

	"gitlab.com/kuzmem/auth/internal/infrastructure/component"
	aservice "gitlab.com/kuzmem/auth/internal/modules/auth/service"
	uservice "gitlab.com/kuzmem/auth/internal/modules/user/service"
	"gitlab.com/kuzmem/auth/internal/storages"
)

type Services struct {
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components, client interface{}) *Services {
	var userService uservice.Userer
	if components.Conf.RPCServer.Type == "JsonRPC" {
		userService = uservice.NewUserServiceJSONRPC(client.(*rpc.Client))
	} else if components.Conf.RPCServer.Type == "GRPC" {
		userService = uservice.NewUserServiceGRPC(client.(upb.UsererClient))
	}

	return &Services{
		UserClientRPC: userService,
		Auth:          aservice.NewAuth(userService, storages.Verify, components),
	}
}
