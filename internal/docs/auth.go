package docs

import (
	"gitlab.com/kuzmem/gateway/pkg/message/auth_mess"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route POST /api/1/auth/register auth registerRequest
// Регистрация пользователя.
// responses:
//   200: registerResponse

// swagger:parameters registerRequest
type registerRequest struct {
	// in:body
	Body auth_mess.RegisterRequest
}

// swagger:response registerResponse
type registerResponse struct {
	// in:body
	Body auth_mess.RegisterResponse
}

// swagger:route POST /api/1/auth/login auth loginRequest
// Авторизация пользователя.
// responses:
//   200: loginResponse

// swagger:parameters loginRequest
type loginRequest struct {
	// in:body
	Body auth_mess.LoginRequest
}

// swagger:response loginResponse
type loginResponse struct {
	// in:body
	Body auth_mess.AuthResponse
}

// swagger:route POST /api/1/auth/refresh auth refreshRequest
// Обновление рефреш токена.
// security:
//   - Bearer: []
// responses:
//   200: refreshResponse

// swagger:response refreshResponse
type refreshRespone struct {
	// in:body
	Body auth_mess.AuthResponse
}

// swagger:route POST /api/1/auth/verify auth verifyRequest
// Верификация почты/телефона пользователя.
// responses:
//   200: verifyResponse

// swagger:parameters verifyRequest
type verifyRequest struct {
	// in:body
	Body auth_mess.VerifyRequest
}

// swagger:response verifyResponse
type verifyResponse struct {
	// in:body
	Body auth_mess.AuthResponse
}
