package storages

import (
	"gitlab.com/kuzmem/auth/internal/db/adapter"
	vstorage "gitlab.com/kuzmem/auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
